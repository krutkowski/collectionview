
#import "Cell.h"

#import "TextFieldCellModel.h"

@interface TextFieldCell : Cell

@property (nonatomic, weak) TextFieldCellModel *cellModel;

@end
