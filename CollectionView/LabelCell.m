
#import "LabelCell.h"

@interface LabelCell ()

@property (nonatomic, strong) UILabel *label;

@end

@implementation LabelCell

@dynamic cellModel;

- (void)setCellModel:(LabelCellModel *)cellModel
{
	[super setCellModel:cellModel];
	self.label.text = cellModel.string;
}

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];

	if (self) {
		self.backgroundColor = [UIColor yellowColor];
		[self setupSubvies];
	}
	
	return self;
}

- (void)setupSubvies
{
	self.label = [UILabel new];
	self.label.translatesAutoresizingMaskIntoConstraints = NO;
	[self.contentView addSubview:self.label];
	[self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.label
																 attribute:NSLayoutAttributeCenterX
																 relatedBy:NSLayoutRelationEqual
																	toItem:self.label.superview
																 attribute:NSLayoutAttributeCenterX
																multiplier:1.f
																  constant:0.f]];
	[self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.label
																 attribute:NSLayoutAttributeCenterY
																 relatedBy:NSLayoutRelationEqual
																	toItem:self.label.superview
																 attribute:NSLayoutAttributeCenterY
																multiplier:1.f
																  constant:0.f]];
}

@end
