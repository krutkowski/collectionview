
#import "TextFieldCellModel.h"

#import "SectionController.h"
#import "TextFieldCell.h"

@implementation TextFieldCellModel

@synthesize hidden, indexPath, controller;

- (Class)cellClass
{
	return TextFieldCell.class;
}

- (CGSize)size
{
	if (self.dualSize) {
		return CGSizeMake(200, 100);
	} else {
		return CGSizeMake(100, 100);
	}
}

- (NSString *)identifier
{
	return @"TextFieldCell";
}

- (void)userDidChangeText:(NSString *)text
{
	self.text = text;
	[self.controller modelDidUpdate:self];
}

- (void)userDidBeginEditingText
{
	[self.controller modelDidBeginUpdate:self];
}

- (NSString *)description
{
	NSString *indexPathString;

	if (self.indexPath) {
		indexPathString = [NSString stringWithFormat:@"%i x %i", (int)self.indexPath.row, (int)self.indexPath.section];
	} else {
		indexPathString = @"NULL";
	}
	return [NSString stringWithFormat:@"<TextFieldCellModel indexPath: %@>", indexPathString];
}

@end
