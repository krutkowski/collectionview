
#import "Cell.h"

#import "LabelCellModel.h"

@interface LabelCell : Cell

@property (nonatomic, weak) LabelCellModel *cellModel;

@end
