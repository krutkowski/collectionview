
#import "Cell.h"

#import "SwitchCellModel.h"

@interface SwitchCell : Cell

@property (nonatomic, weak) SwitchCellModel *cellModel;

@end
