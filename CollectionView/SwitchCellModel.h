
#import "CellModel.h"

@interface SwitchCellModel : NSObject <CellModel>

@property (nonatomic) BOOL on;

- (void)userDidChangeOn:(BOOL)on;

@end
