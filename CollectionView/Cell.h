
@import UIKit;

@protocol CellModel;

@interface Cell : UICollectionViewCell

@property (nonatomic, weak) id <CellModel> cellModel;

@end
