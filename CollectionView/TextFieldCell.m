
#import "TextFieldCell.h"

@interface TextFieldCell ()

@property (nonatomic, strong) UITextField *textField;

@end

@implementation TextFieldCell

@dynamic cellModel;

- (void)setCellModel:(TextFieldCellModel *)cellModel
{
	[super setCellModel:cellModel];

	self.textField.text = cellModel.text;
}

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];

	if (self) {
		self.backgroundColor = [UIColor greenColor];
		[self setupSubvies];
	}

	return self;
}

- (void)setupSubvies
{
	self.textField = [UITextField new];
	self.textField.translatesAutoresizingMaskIntoConstraints = NO;
	[self.contentView addSubview:self.textField];
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[textField]-|"
																			 options:0
																			 metrics:nil
																			   views:@{@"textField": self.textField}]];
	[self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.textField
																 attribute:NSLayoutAttributeCenterY
																 relatedBy:NSLayoutRelationEqual
																	toItem:self.textField.superview
																 attribute:NSLayoutAttributeCenterY
																multiplier:1.f
																  constant:0.f]];
	self.textField.borderStyle = UITextBorderStyleBezel;
	[self.textField addTarget:self action:@selector(userDidChangeText) forControlEvents:UIControlEventEditingChanged];
	[self.textField addTarget:self action:@selector(userDidBeginEditingText) forControlEvents:UIControlEventEditingDidBegin];
}

- (void)userDidChangeText
{
	[self.cellModel userDidChangeText:self.textField.text];
}

- (void)userDidBeginEditingText
{
	[self.cellModel userDidBeginEditingText];
}

@end
