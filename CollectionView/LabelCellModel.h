
#import "CellModel.h"

@interface LabelCellModel : NSObject <CellModel>

@property (nonatomic, strong) NSString *string;

@end
