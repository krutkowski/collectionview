
#import "CellModel.h"

@interface TextFieldCellModel : NSObject <CellModel>

@property (nonatomic, copy) NSString *text;
@property (nonatomic) BOOL dualSize;

- (void)userDidChangeText:(NSString *)text;
- (void)userDidBeginEditingText;

@end
