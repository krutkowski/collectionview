
#import "SectionController.h"

#import "Cell.h"
#import "CellModel.h"

@interface SectionController ()

@property (nonatomic, strong) NSMutableSet *cellsToBeReloaded;
@property (nonatomic, strong) NSMutableSet *cellsToBeInserted;
@property (nonatomic, strong) NSMutableSet *cellsToBeDeleted;

@end

@implementation SectionController

- (NSMutableSet *)cellsToBeReloaded
{
	if (!_cellsToBeReloaded) {
		_cellsToBeReloaded = [NSMutableSet new];
	}

	return _cellsToBeReloaded;
}

- (NSMutableSet *)cellsToBeInserted
{
	if (!_cellsToBeInserted) {
		_cellsToBeInserted = [NSMutableSet new];
	}

	return _cellsToBeInserted;
}


- (NSMutableSet *)cellsToBeDeleted
{
	if (!_cellsToBeDeleted) {
		_cellsToBeDeleted = [NSMutableSet new];
	}

	return _cellsToBeDeleted;
}

- (void)setCellModels:(NSArray *)cellModels
{
	_cellModels = cellModels;

	for (id <CellModel> cellModel in cellModels) {
		cellModel.controller = self;
		[(NSObject *)cellModel addObserver:self forKeyPath:@"hidden" options:NSKeyValueObservingOptionOld context:NULL];
	}
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if ([self.cellModels containsObject:object]) {
		BOOL previousValue = [change[NSKeyValueChangeOldKey] boolValue];
		id <CellModel> cellModel = object;
		BOOL currentValue = cellModel.hidden;

		if (currentValue != previousValue) {
			if (cellModel.hidden) {
				if ([self.cellsToBeInserted containsObject:cellModel]) {
					[self.cellsToBeInserted removeObject:cellModel];
				} else {
					[self.cellsToBeDeleted addObject:cellModel];
				}
			} else {
				if ([self.cellsToBeDeleted containsObject:cellModel]) {
					[self.cellsToBeDeleted removeObject:cellModel];
				} else {
					[self.cellsToBeInserted addObject:cellModel];
				}
			}
		}
	} else {
		[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
	}
}

- (NSInteger)numberOfVisibleCells
{
	NSUInteger visibleCellsCount = 0;

	for (id <CellModel> cellModel in self.cellModels) {
		if (!cellModel.hidden) {
			visibleCellsCount++;
		}
	}

	return visibleCellsCount;
}

- (NSInteger)numberOfCells
{
	return self.cellModels.count;
}

- (NSSet *)allIdentifiers
{
	NSMutableSet *identifiers = [NSMutableSet set];
	for (id <CellModel> cellModel in self.cellModels) {
		[identifiers addObject:cellModel.identifier];
	}

	return identifiers;
}

- (Class)classForIdentifier:(NSString *)identifier
{
	for (id <CellModel> cellModel in self.cellModels) {
		if ([cellModel.identifier isEqualToString:identifier]) {
			return cellModel.cellClass;
		}
	}

	@throw [NSException exceptionWithName:@"You're moron"
								   reason:@"You know why"
								 userInfo:nil];
}

- (NSString *)identifierForVisibleCellAtIndex:(NSInteger)index
{
	NSInteger visibleCellIndex = 0;

	for (id <CellModel> cellModel in self.cellModels) {
		if (!cellModel.hidden) {
			if (visibleCellIndex == index) {
				return cellModel.identifier;
			}

			visibleCellIndex++;
		}
	}

	@throw [NSException exceptionWithName:@"You're moron"
								   reason:@"You know why"
								 userInfo:nil];
}

- (id <CellModel>)cellModelForVisibleCellAtIndex:(NSInteger)index
{
	NSInteger visibleCellIndex = 0;

	for (id <CellModel> cellModel in self.cellModels) {
		if (!cellModel.hidden) {
			if (visibleCellIndex == index) {
				return cellModel;
			}

			visibleCellIndex++;
		}
	}

	@throw [NSException exceptionWithName:@"You're moron"
								   reason:@"You know why"
								 userInfo:nil];
}

- (NSUInteger)visibleIndexForModel:(id <CellModel>)model
{
	NSUInteger index = 0;

	for (id <CellModel> cellModel in self.cellModels) {
		if (cellModel == model) {
			break;
		}

		if (!cellModel.hidden) {
			index++;
		}
	}

	return index;
}

- (void)modelDidUpdate:(id <CellModel>)model
{
	
}

- (void)modelDidBeginUpdate:(id <CellModel>)model
{
	
}

- (void)dealloc
{
	for (id <CellModel> cellModel in self.cellModels) {
		[(NSObject *)cellModel removeObserver:self
								   forKeyPath:@"hidden"];
	}
}

@end
