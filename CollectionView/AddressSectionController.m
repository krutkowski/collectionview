
#import "AddressSectionController.h"

#import "LabelCellModel.h"
#import "SwitchCellModel.h"
#import "TextFieldCellModel.h"

@interface AddressSectionController ()

@property (nonatomic, strong) TextFieldCellModel *fullCardNumberCellModel;
@property (nonatomic, strong) TextFieldCellModel *shortCardNumberCellModel;
@property (nonatomic, strong) TextFieldCellModel *expiryDateCellModel;
@property (nonatomic, strong) TextFieldCellModel *cvvCellModel;
@property (nonatomic, strong) TextFieldCellModel *nameCellModel;
@property (nonatomic, strong) SwitchCellModel *provideIssueDateCellModel;
@property (nonatomic, strong) TextFieldCellModel *issueDateCellModel;

@property (nonatomic) BOOL isDisplayingDetailedCardInput;

@end

@implementation AddressSectionController

- (TextFieldCellModel *)fullCardNumberCellModel
{
	if (!_fullCardNumberCellModel) {
		_fullCardNumberCellModel = [TextFieldCellModel new];
		_fullCardNumberCellModel.dualSize = YES;
	}

	return _fullCardNumberCellModel;
}

- (TextFieldCellModel *)shortCardNumberCellModel
{
	if (!_shortCardNumberCellModel) {
		_shortCardNumberCellModel = [TextFieldCellModel new];
		_shortCardNumberCellModel.hidden = YES;
	}

	return _shortCardNumberCellModel;
}

- (TextFieldCellModel *)expiryDateCellModel
{
	if (!_expiryDateCellModel) {
		_expiryDateCellModel = [TextFieldCellModel new];
		_expiryDateCellModel.hidden = YES;
	}

	return _expiryDateCellModel;
}

- (TextFieldCellModel *)cvvCellModel
{
	if (!_cvvCellModel) {
		_cvvCellModel = [TextFieldCellModel new];
		_cvvCellModel.hidden = YES;
	}

	return _cvvCellModel;
}

- (TextFieldCellModel *)nameCellModel
{
	if (!_nameCellModel) {
		_nameCellModel = [TextFieldCellModel new];
		_nameCellModel.hidden = YES;
	}

	return _nameCellModel;
}

- (SwitchCellModel *)provideIssueDateCellModel
{
	if (!_provideIssueDateCellModel) {
		_provideIssueDateCellModel = [SwitchCellModel new];
	}

	return _provideIssueDateCellModel;
}

- (TextFieldCellModel *)issueDateCellModel
{
	if (!_issueDateCellModel) {
		_issueDateCellModel = [TextFieldCellModel new];
		_issueDateCellModel.hidden = YES;
	}

	return _issueDateCellModel;
}

- (instancetype)init
{
	self = [super init];

	if (self) {
		self.cellModels = @[
							[self fullCardNumberCellModel],
							[self shortCardNumberCellModel],
							[self expiryDateCellModel],
							[self cvvCellModel],
							[self nameCellModel],
							[self provideIssueDateCellModel],
							[self issueDateCellModel]
							];
	}

	return self;
}

- (void)setIsDisplayingDetailedCardInput:(BOOL)isDisplayingDetailedCardInput
{
	_isDisplayingDetailedCardInput = isDisplayingDetailedCardInput;

	if (!isDisplayingDetailedCardInput) {
		self.fullCardNumberCellModel.hidden = NO;
		self.shortCardNumberCellModel.hidden = YES;
		self.expiryDateCellModel.hidden = YES;
		self.cvvCellModel.hidden = YES;
		self.nameCellModel.hidden = YES;
	} else {
		self.fullCardNumberCellModel.hidden = YES;
		self.shortCardNumberCellModel.hidden = NO;
		self.expiryDateCellModel.hidden = NO;
		self.cvvCellModel.hidden = NO;
		self.nameCellModel.hidden = NO;
	}
}

- (void)modelDidUpdate:(id<CellModel>)model
{
	self.editing = YES;
	if (model == self.fullCardNumberCellModel) {
		if (self.fullCardNumberCellModel.text.length == 16) {
			self.shortCardNumberCellModel.text = [self.fullCardNumberCellModel.text substringFromIndex:self.fullCardNumberCellModel.text.length - 4];
			self.isDisplayingDetailedCardInput = YES;
		}
	} else if (model == self.provideIssueDateCellModel) {
		if (self.provideIssueDateCellModel.on) {
			self.issueDateCellModel.hidden = NO;
		} else {
			self.issueDateCellModel.hidden = YES;
		}
	}
	self.editing = NO;
}

- (void)modelDidBeginUpdate:(id<CellModel>)model
{
	self.editing = YES;
	if (model == self.shortCardNumberCellModel) {
		self.isDisplayingDetailedCardInput = NO;
	}
	self.editing = NO;
}

@end
