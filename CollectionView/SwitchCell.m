
#import "SwitchCell.h"

@interface SwitchCell ()

@property (nonatomic, strong) UISwitch *switchControl;

@end

@implementation SwitchCell

@dynamic cellModel;

- (void)setCellModel:(SwitchCellModel *)cellModel
{
	[super setCellModel:cellModel];

	self.switchControl.on = cellModel.on;
}

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];

	if (self) {
		self.backgroundColor = [UIColor grayColor];
		[self setupSubvies];
	}

	return self;
}

- (void)setupSubvies
{
	self.switchControl = [UISwitch new];
	self.switchControl.translatesAutoresizingMaskIntoConstraints = NO;
	[self.contentView addSubview:self.switchControl];
	[self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.switchControl
																 attribute:NSLayoutAttributeCenterX
																 relatedBy:NSLayoutRelationEqual
																	toItem:self.switchControl.superview
																 attribute:NSLayoutAttributeCenterX
																multiplier:1.f
																  constant:0.f]];
	[self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.switchControl
																 attribute:NSLayoutAttributeCenterY
																 relatedBy:NSLayoutRelationEqual
																	toItem:self.switchControl.superview
																 attribute:NSLayoutAttributeCenterY
																multiplier:1.f
																  constant:0.f]];
	[self.switchControl addTarget:self action:@selector(userDidChangeOn) forControlEvents:UIControlEventValueChanged];
}

- (void)userDidChangeOn
{
	[self.cellModel userDidChangeOn:self.switchControl.on];
}

@end
