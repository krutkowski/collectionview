
#import "CollectionViewController.h"

#import "Cell.h"
#import "CellModel.h"
#import "SectionController.h"

@interface CollectionViewController () <UICollectionViewDelegateFlowLayout>

@property (nonatomic) BOOL performingUpdate;
@property (nonatomic) BOOL chainUpdates;

@end

@implementation CollectionViewController

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
	SectionController *sectionController = self.sectionControllers[indexPath.section];
	id <CellModel> cellModel = [sectionController cellModelForVisibleCellAtIndex:indexPath.row];

	return cellModel.size;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
	return UIEdgeInsetsMake(50, 20, 50, 20);
}

- (void)setSectionControllers:(NSArray *)sectionControllers
{
	if (_sectionControllers != nil) {
		@throw [NSException exceptionWithName:@"You're moron"
									   reason:@"You know why"
									 userInfo:nil];
	}

	_sectionControllers = sectionControllers;

	for (SectionController *sectionController in sectionControllers) {
		NSSet *identifiers = sectionController.allIdentifiers;

		for (NSString *identifier in identifiers) {
			Class cellClass = [sectionController classForIdentifier:identifier];
			[self.collectionView registerClass:cellClass
					forCellWithReuseIdentifier:identifier];
		}

		[sectionController addObserver:self
							forKeyPath:@"editing"
							   options:NSKeyValueObservingOptionNew
							   context:NULL];
	}
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
	return self.sectionControllers.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
	SectionController *controller = self.sectionControllers[section];

	return controller.numberOfVisibleCells;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
	SectionController *sectionController = self.sectionControllers[indexPath.section];

	NSString *identifier = [sectionController identifierForVisibleCellAtIndex:indexPath.row];

	Cell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier
														   forIndexPath:indexPath];

	id <CellModel> cellModel = [sectionController cellModelForVisibleCellAtIndex:indexPath.row];
	cellModel.indexPath = indexPath;
	cell.cellModel = cellModel;

	return cell;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if ([self.sectionControllers containsObject:object]) {
		[NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(updateCollectionViewIfNeeded) object:nil];
		[self performSelector:@selector(updateCollectionViewIfNeeded) withObject:nil afterDelay:0.01];
	} else {
		[super observeValueForKeyPath:keyPath
							 ofObject:object
							   change:change
							  context:context];
	}
}

- (void)updateCollectionViewIfNeeded
{
	for (SectionController *sectionController in self.sectionControllers) {
		if (sectionController.editing) {
			return;
		}
	}

	if (self.performingUpdate) {
		self.chainUpdates = YES;
		return;
	}

	self.performingUpdate = YES;

	[self.collectionView performBatchUpdates:^{

		for (SectionController *sectionController in self.sectionControllers) {
			NSUInteger sectionIndex = [self.sectionControllers indexOfObject:sectionController];
			for (id <CellModel> cellModel in sectionController.cellsToBeInserted) {
				NSInteger index = [sectionController visibleIndexForModel:cellModel];
				NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:sectionIndex];
				[self.collectionView insertItemsAtIndexPaths:@[indexPath]];
			}
			[sectionController.cellsToBeInserted removeAllObjects];


			for (id <CellModel> cellModel in sectionController.cellsToBeDeleted) {
				[self.collectionView deleteItemsAtIndexPaths:@[cellModel.indexPath]];
			}
			[sectionController.cellsToBeDeleted removeAllObjects];

			for (id <CellModel> cellModel in sectionController.cellsToBeReloaded) {
				[self.collectionView reloadItemsAtIndexPaths:@[cellModel.indexPath]];
			}
			[sectionController.cellsToBeReloaded removeAllObjects];

			for (NSUInteger i = 0; i < sectionController.numberOfVisibleCells; i++) {
				id <CellModel> cellModel = [sectionController cellModelForVisibleCellAtIndex:i];
				cellModel.indexPath = [NSIndexPath indexPathForRow:i inSection:sectionIndex];
			}
		}
	} completion:^(BOOL finished) {
		if (finished) {
			self.performingUpdate = NO;
			if (self.chainUpdates) {
				self.chainUpdates = NO;

				[self updateCollectionViewIfNeeded];
			}
		}
	}];
}

- (void)dealloc
{
	for (SectionController *sectionController in self.sectionControllers) {
		[sectionController removeObserver:self
							   forKeyPath:@"editing"];
	}
}

@end
