
#import "SwitchCellModel.h"

#import "SectionController.h"
#import "SwitchCell.h"

@implementation SwitchCellModel

@synthesize hidden, indexPath, controller;

- (Class)cellClass
{
	return SwitchCell.class;
}

- (CGSize)size
{
	return CGSizeMake(100, 100);
}

- (NSString *)identifier
{
	return @"SwitchCell";
}

- (void)userDidChangeOn:(BOOL)on
{
	self.on = on;
	[self.controller modelDidUpdate:self];
}

- (NSString *)description
{
	NSString *indexPathString;

	if (self.indexPath) {
		indexPathString = [NSString stringWithFormat:@"%i x %i", (int)self.indexPath.row, (int)self.indexPath.section];
	} else {
		indexPathString = @"NULL";
	}
	return [NSString stringWithFormat:@"<SwitchCellModel indexPath: %@>", indexPathString];
}

@end
