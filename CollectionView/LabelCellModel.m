
#import "LabelCellModel.h"

#import "LabelCell.h"

@implementation LabelCellModel

@synthesize hidden, indexPath, controller;

- (Class)cellClass
{
	return LabelCell.class;
}

- (CGSize)size
{
	return CGSizeMake(100, 100);
}

- (NSString *)identifier
{
	return @"LabelCell";
}

- (NSString *)description
{
	NSString *indexPathString;

	if (self.indexPath) {
		indexPathString = [NSString stringWithFormat:@"%i x %i", (int)self.indexPath.row, (int)self.indexPath.section];
	} else {
		indexPathString = @"NULL";
	}
	return [NSString stringWithFormat:@"<LabelCellModel indexPath: %@>", indexPathString];
}

@end
