
@import UIKit;

@protocol CellModel;

@interface SectionController : NSObject

@property (nonatomic) BOOL editing;
@property (nonatomic, strong, readonly) NSMutableSet *cellsToBeReloaded;
@property (nonatomic, strong, readonly) NSMutableSet *cellsToBeInserted;
@property (nonatomic, strong, readonly) NSMutableSet *cellsToBeDeleted;

// protected
@property (nonatomic, strong) NSArray *cellModels;

- (NSInteger)numberOfVisibleCells;
- (NSInteger)numberOfCells;
- (NSSet *)allIdentifiers;
- (Class)classForIdentifier:(NSString *)identifier;
- (NSString *)identifierForVisibleCellAtIndex:(NSInteger)index;
- (id <CellModel>)cellModelForVisibleCellAtIndex:(NSInteger)index;
- (NSUInteger)visibleIndexForModel:(id <CellModel>)model;
- (void)modelDidUpdate:(id <CellModel>)model;
- (void)modelDidBeginUpdate:(id <CellModel>)model;

@end
