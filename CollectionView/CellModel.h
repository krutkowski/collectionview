
@import UIKit;

@class SectionController;

@protocol CellModel <NSObject>

@property (nonatomic, weak) SectionController *controller;
@property (nonatomic, strong, readonly) Class cellClass;
@property (nonatomic, strong, readonly) NSString *identifier;
@property (nonatomic) BOOL hidden;

// assigned only by collection view
@property (nonatomic) NSIndexPath *indexPath;
@property (nonatomic, readonly) CGSize size;

@end
