//
//  ProductVC.m
//  CollectionView
//
//  Created by Krzysztof Rutkowski on 28/08/2015.
//  Copyright (c) 2015 Krzysztof Rutkowski. All rights reserved.
//

#import "ProductVC.h"

#import "AddressSectionController.h"

@interface ProductVC ()

@end

@implementation ProductVC

- (void)awakeFromNib
{
	[super awakeFromNib];

	self.sectionControllers = @[
								[AddressSectionController new],
								[AddressSectionController new],
								//[FirstSectionController new],
								//[FirstSectionController new]
								];
}

@end
