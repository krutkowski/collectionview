
@import UIKit;

@interface CollectionViewController : UICollectionViewController

@property (nonatomic, strong) NSArray *sectionControllers;

@end
